<div class="ideas" style="background-image: url(img/shoot-cannon.png)">
	<div class="container">
		<?php

			include './configuration/connection.php';

			$sql = 'SELECT * FROM ml_projects ORDER BY votes DESC';
			$result = $conn->query($sql);

			if ($result->num_rows > 0) {
			    // output data of each row
			    while($row = $result->fetch_assoc()) {
						$description = $row["description"];
						if(strlen($description) >= 300){
							$description = substr($description, 0 ,300);
							$description .= "...";
						}
						echo '<div class="idea">';
						echo '<div class="idea-image" style="background-image: url(img/test.jpg)"></div>';
						echo '<div class="idea-title">' . $row["title"] . ' - '.$row["votes"].'</div>';
						echo '<div class="idea-description"><p>'. $description .'</p></div>';
						echo '<a href="detail.php?id='. $row["project_id"] .'" class="button">Bekijk idee</a>';
						echo '</div>';
			    }
			} else {
			    echo "0 results";
			}
			$conn->close();

		?>
	</div>
</div>
