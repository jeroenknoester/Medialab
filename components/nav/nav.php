<?php session_start();?>

<nav>
	<div class="container">
		<a href="index.php" class="logo"></a>
		<ul>
			<li><a href="gamerules.php">Spelregels</a></li>

			<li><a href="index.php">Overview</a></li>
			<?php
			if(!isset($_SESSION["user_email"]) && !isset($_SESSION["user_password"])){
				echo '<li><a href="login.php">Inloggen</a></li>';
				echo '<li><a href="register.php">Registreren</a></li>';
			} else {
				echo'<li><a href="participate.php">Meedoen</a></li>';
				echo '<li><a href="PHP/logout.php">Logout</a></li>';
			} ?>
		</ul>
	</div>
</nav>
