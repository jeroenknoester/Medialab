<div class="register-form" style="background-image: url(img/shoot-cannon.png)">
	<div class="container">
		
		<div class="register-form-wrapper">
			<h1>Registeren</h1>
			
			<form action="PHP/register_post.php" method="POST">			    
			    <input type="text" class="form-control" name="voornaam" id="voornaam" aria-describedby="voornaam" placeholder="Voornaam">
			    <input type="text" class="form-control" name="achternaam" id="achternaam" aria-describedby="emailHelp" placeholder="Achternaam">
			    <input type="text" class="form-control" name="team" id="team" aria-describedby="emailHelp" placeholder="Team naam">
			    <input type="email" class="form-control" name="email" id="email" aria-describedby="emailHelp" placeholder="Email">
			    <input type="password" class="form-control" name="password1" id="password1" placeholder="Wachtwoord">
			    <input type="password" class="form-control" name="password2" id="password2" placeholder="Wachtwoord controle">

			  <button type="register" class="register-button">Registreer</button>
			</form>

		</div>

	</div>
</div>
