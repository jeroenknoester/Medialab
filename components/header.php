<!doctype html>

<html lang="en">
<head>
	<meta charset="utf-8">

	<title>The HTML5 Herald</title>
	<meta name="description" content="The HTML5 Herald">
	<meta name="author" content="SitePoint">

	<!-- Default -->
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="components/nav/nav.css">
	<!-- Home -->
	<link rel="stylesheet" href="components/banner/banner.css">
	<link rel="stylesheet" href="components/ideas/ideas.css">
	<!-- Detail -->
	<link rel="stylesheet" href="components/detail-banner/detail-banner.css">
	<link rel="stylesheet" href="components/detail-content/detail-content.css">
	<!-- Gamerules -->
	<link rel="stylesheet" href="components/gamerules-banner/gamerules-banner.css">
	<link rel="stylesheet" href="components/gamerules-content/gamerules-content.css">
	<!-- Participate -->
	<link rel="stylesheet" href="components/participate-banner/participate-banner.css">
	<link rel="stylesheet" href="components/participate-content/participate-content.css">
	<!-- Login -->
	<link rel="stylesheet" href="components/login-form/login-form.css">
	<!-- Register -->
	<link rel="stylesheet" href="components/register-form/register-form.css">

	<!--[if lt IE 9]>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
	<![endif]-->
</head>

<body>
