<?php

	include 'components/header.php';
	include 'components/nav/nav.php';
	include 'components/participate-banner/participate-banner.php';
	include 'components/participate-content/participate-content.php';

	if(!isset($_SESSION["user_email"]) && !isset($_SESSION["user_password"])){
		header('location: index.php');
	}
?>
</body>
